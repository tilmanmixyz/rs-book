// use rand::Rng;
use std::cmp::Ordering;
use std::collections::hash_map::{self, DefaultHasher};
use std::hash::{BuildHasher, Hasher};
use std::io;
use std::ops::Range;

struct Rng {
    hasher: DefaultHasher,
}

impl Rng {
    pub fn new() -> Self {
        Self {
            hasher: hash_map::RandomState::new().build_hasher(),
        }
    }

    pub fn u64_range(&self, range: Range<u64>) -> u64 {
        self.hasher.finish() % (range.end - range.start) + range.start
    }
}

fn main() {
    println!("Huess the number between 1 and including and 100");
    // let rand_num = rand::thread_rng().gen_range(1..=100);
    let rng = Rng::new();
    let rand_num = rng.u64_range(1..101);
    let mut guesses = 0;
    loop {
        println!("Please input your guess");

        let mut guess = String::new();

        match io::stdin().read_line(&mut guess) {
            Ok(string) => string,
            Err(_) => {
                println!("Could not read input");
                continue;
            }
        };

        let guess = match guess.trim().parse::<u64>() {
            Ok(num) => num,
            Err(_) => {
                println!("Please input a number");
                continue;
            }
        };

        guesses = guesses + 1;

        match guess.cmp(&rand_num) {
            Ordering::Less => println!("Too small"),
            Ordering::Greater => println!("To Big"),
            Ordering::Equal => {
                println!("You won, it took you {guesses} guesses.");
                break;
            }
        };
    }
}
