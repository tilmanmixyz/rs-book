struct Temp {
    degree: f64,
}

impl Temp {
    fn new(degree: f64) -> Self {
        Self { degree: (degree) }
    }

    fn to_celsius(&self) -> f64 {
        (self.degree - 32.0) / 1.8
    }

    fn to_fahrenheit(&self) -> f64 {
        self.degree * 1.8 + 32.0
    }
}

fn main() {
    let c = Temp::new(37.0);
    println!(
        "{} degrees celsius are {} degrees fahrenheit",
        c.degree,
        c.to_fahrenheit()
    );

    let f = Temp::new(-40.0);
    println!(
        "{} degrees fahrenheit are {} degrees celsius",
        f.degree,
        f.to_celsius()
    );
}
