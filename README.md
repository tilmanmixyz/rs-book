# rs-book

[![dependency status](https://deps.rs/repo/codeberg/tilmanmixyz/rs-book/status.svg)](https://deps.rs/repo/codeberg/tilmanmixyz/rs-book)

My unnecessarily complex solutions to the tasks at the end of each chapter in the [rust book](https://docs.rust-lang.org/book).
