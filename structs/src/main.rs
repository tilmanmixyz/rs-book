use async_trait::async_trait;

struct Rectangle {
    width: i32,
    height: i32,
}

struct Triangle {
    a: i32,
    b: i32,
    c: i32,
    height: i32,
}

#[async_trait]
trait Area {
    async fn area(&self) -> i32;
    async fn circumference(&self) -> i32;
}

#[async_trait]
impl Area for Triangle {
    async fn area(&self) -> i32 {
        (self.a * self.height) / 2
    }
    async fn circumference(&self) -> i32 {
        self.a + self.b + self.c
    }
}

impl Triangle {
    async fn new(a: i32, b: i32, c: i32, height: i32) -> Self {
        Self { a, b, c, height }
    }
}

#[async_trait]
impl Area for Rectangle {
    async fn area(&self) -> i32 {
        self.width * self.height
    }

    async fn circumference(&self) -> i32 {
        (self.width + self.height) * 2
    }
}

impl Rectangle {
    async fn new(width: i32, height: i32) -> Rectangle {
        Rectangle { width, height }
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

#[async_std::main]
async fn main() {
    let rec1 = Rectangle::new(50, 30).await;
    let rec2 = Rectangle::new(60, 40).await;
    let area = rec1.area().await;
    println!(
        "A rectangle of dimensions {}x{}cm has an area of {}cm^2",
        rec1.width, rec1.height, area,
    );
    if rec2.can_hold(&rec1) {
        println!("rec2 can hold rec1");
    } else {
        println!("rec2 can not hold rec1");
    }

    let tri = Triangle::new(2, 3, 4, 5).await;
    println!("A: {}, U: {}", tri.area().await, tri.circumference().await);
    println!(
        "A: {}, U: {}",
        rec1.area().await,
        rec1.circumference().await
    );
}
