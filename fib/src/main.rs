fn fib(num: i32) -> i64 {
    match num {
        1 => 1,
        0 => 1,
        _ => fib(num - 1) + fib(num - 2),
    }
}

fn main() {
    println!("{}", fib(1));
    println!("{}", fib(3));
    println!("{}", fib(5));
}
