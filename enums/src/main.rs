#[derive(Debug)]
enum IpAddrType {
    V6,
    V4,
}

#[derive(Debug)]
struct IpAddrStruct {
    version: IpAddrType,
    addr: String,
}

impl IpAddrStruct {
    fn new(version: IpAddrType, addr: String) -> IpAddrStruct {
        IpAddrStruct { version, addr }
    }
}

impl Default for IpAddrStruct {
    fn default() -> Self {
        Self {
            version: IpAddrType::V4,
            addr: "127.0.0.1".to_string(),
        }
    }
}

#[derive(Debug)]
enum IpAddrFullEnum {
    V4(String),
    V6(String),
}

#[derive(Debug)]
struct IpAddr {
    version: IpAddrType,
    addr: String,
    port: i32,
}

impl Default for IpAddr {
    fn default() -> Self {
        Self {
            version: IpAddrType::V4,
            addr: "127.0.0.1".to_string(),
            port: 80,
        }
    }
}

impl IpAddr {
    fn new(version: IpAddrType, addr: String, port: i32) -> Self {
        Self {
            version,
            addr,
            port,
        }
    }
}

#[derive(Debug)]
struct StdLibIpv4Addr {
    first: i32,
    second: i32,
    third: i32,
    fourth: i32,
}

impl StdLibIpv4Addr {
    fn new(first: i32, second: i32, third: i32, fourth: i32) -> Self {
        Self {
            first,
            second,
            third,
            fourth,
        }
    }
}

#[derive(Debug)]
struct StdLibIpv6Addr {
    addr: String,
}

impl StdLibIpv6Addr {
    fn new(addr: String) -> Self {
        Self { addr }
    }
}

#[derive(Debug)]
enum StdLibIpAddr {
    V4(StdLibIpv4Addr),
    V6(StdLibIpv6Addr),
}

fn main() {
    let addrv6 = IpAddrStruct::new(IpAddrType::V6, "::1".to_string());
    dbg!(addrv6);
    let addrv4 = IpAddrFullEnum::V4("127.0.0.1".to_string());
    dbg!(addrv4);
    let ipaddr = IpAddr::new(IpAddrType::V6, "::1".to_string(), 8080);
    println!(
        "version: {:?}, addr: {}, port: {}",
        ipaddr.version, ipaddr.addr, ipaddr.port
    );
    let std_lib_v4 = StdLibIpAddr::V4(StdLibIpv4Addr::new(127, 0, 0, 1));
    dbg!(std_lib_v4);
    let std_lib_v6 = StdLibIpAddr::V6(StdLibIpv6Addr::new("::1".to_string()));
    dbg!(std_lib_v6);
}
