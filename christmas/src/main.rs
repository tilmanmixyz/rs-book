struct Christmas {
    day: i32,
}

impl ToString for Christmas {
    fn to_string(&self) -> String {
        match self.day {
            1 => "first".to_string(),
            2 => "second".to_string(),
            3 => "third".to_string(),
            4 => "fourth".to_string(),
            5 => "fifth".to_string(),
            6 => "sixth".to_string(),
            7 => "seventh".to_string(),
            8 => "eighth".to_string(),
            9 => "ninth".to_string(),
            10 => "tenth".to_string(),
            11 => "eleventh".to_string(),
            12 => "twelfth".to_string(),
            _ => String::new(),
        }
    }
}

impl Christmas {
    fn print(&self) {
        println!(
            "\nOn the {} day of christmas, my true love sent to me",
            self.day.to_string()
        );

        match self.day {
            1 => Christmas::first(),
            2 => Christmas::second(),
            3 => Christmas::third(),
            4 => Christmas::fourth(),
            5 => Christmas::fifth(),
            6 => Christmas::sixth(),
            7 => Christmas::seventh(),
            8 => Christmas::eighth(),
            9 => Christmas::ninth(),
            10 => Christmas::tenth(),
            11 => Christmas::eleventh(),
            12 => Christmas::twelfth(),
            _ => println!(""),
        }
    }

    fn first() {
        println!("A partridge in a pear tree");
    }

    fn second() {
        println!("Two turtle doves, and");
        Christmas::first();
    }

    fn third() {
        println!("Three french hens");
        Christmas::second();
    }

    fn fourth() {
        println!("Four calling birds");
        Christmas::third();
    }

    fn fifth() {
        println!("Five golden rings");
        Christmas::fourth();
    }

    fn sixth() {
        println!("Six geese a-laying");
        Christmas::fifth();
    }

    fn seventh() {
        println!("Seven swans a-swimming");
        Christmas::sixth();
    }

    fn eighth() {
        println!("Eight maids a-milking");
        Christmas::seventh();
    }

    fn ninth() {
        println!("Nine ladies dancing");
        Christmas::eighth();
    }

    fn tenth() {
        println!("Ten lords a-leaping");
        Christmas::ninth();
    }

    fn eleventh() {
        println!("Eleven pipers piping");
        Christmas::tenth();
    }

    fn twelfth() {
        println!("Twelve drummers drumming");
        Christmas::eleventh();
    }
}

fn main() {
    for i in 1..=12 {
        let christmas = Christmas { day: i };
        christmas.print();
    }
}
